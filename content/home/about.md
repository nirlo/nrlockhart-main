---
title: "About"
weight: 8
image: "img/shattered.jpg"
---

Online, I typically go by Nirlo or Nirlock. I am DevOps Engineer, I love stories of most kinds, the ones that engage the audience and don't let you go.

I'm a Certified Kubernetes Application Developer and I am currently working on becoming a Certified Kubernetes Administrator (I'm pretty much am one, but hey, I like letters on my name)

I have a background in Linguistics and Pyschology and have expanded into technology, finding the places where work and people meet and finding the best process between them. 

I build monitoring systems, expand on Kubernetes, build CI/CD workflows, write engaging documentation, Collaborate with all members of development, and write stories. 
