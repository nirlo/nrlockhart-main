---
title: "Blogs"
weight: 5
---
# [Tech](https://tech.nrlockhart.ca)

Rants and information on different technologies related to software and DevOps Development

# [Writing](https://blog.nrlockhart.ca/posts/)

Short stories, Articles, and Rants
