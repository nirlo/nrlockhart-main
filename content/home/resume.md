---
title: "Background"
weight: 10
---
### Work Experience

#### __DevOps Engineer__
__Natural Partners Fullscript__

Responsible for all aspects of software delivery and techincal operations

Develop internal tools in Golang

Troubleshoot Kubernetes infrastructure

Monitor platform health

Develop CI/CD processes

Developing new platform delivery systems

Ruby/shell scripting

Reducing silos of knowledge

Creating infrastructure as Code


#### __SDE Developer__
__Ribbon Commuincations__

Building system dashboards for observing RHEL servers

Server orchestration using Ansible

Managing virtual machines using KVM and QEMU

Creating automation scripts using Expect and Shell

#### __Inside Sales__
__Roger Communications__

Communicating with clients

Guiding clients to sale through needs based analysis

### Education

#### __Algonquin College - School of Advanced Technology__
**Computer Programmer** Diploma, Dean's Honor List

Graduated 2019

Program for workplace ready programming and practical applications

__Courses__

Web Programming

Advanced Database

Programming Language Research(Golang)

Data Structures

Network Programming

Object Oriented Programming with Design Patterns

#### __Dalhousie University__
Bachelor of Arts, **Major Linguistics, Minor Psychology**

Graduated 2015

Focus on Behavioural Linguistics and Psychology. 

__Courses__

Inferential Statistics

Language and Literacy

Behavioral Psychology

Advanced Animal Communications

Advanced Phonology

### Volunteering

#### OICH Hackathon
[Otawa Inner City Health Hackathon](https://github.com/Fullscript/oich-trailer)

Helped develop new website *The Trailer 2.0*, aiding in delivering health care to homeless and street communities in Lowertown, Ottawa. Built during weekend Hackathon

 Built with: Ruby 2.5.3, Rails 5.2.3, Bulma

### Skills

Kubernetes

AWS

Golang Development

Ruby Scripting

Python Scripting

Infrastructure as Code With Terraform

Infrastructure Design

Gitlab CI/CD

Ansible

System Observability